package id.ac.uinjkt.spmb.uploadraport.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nilai {
    private Long id;
    private String noUjian;
    private String semester;
    private String kode;
    private Double nilai;
}
