package id.ac.uinjkt.spmb.uploadraport.dao;

import id.ac.uinjkt.spmb.uploadraport.entity.TempNilai;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempNilaiDao extends JpaRepository<TempNilai, Long> {

}
