package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "public", name = "biodata_calon_mahasiswa")
public class CalonMahasiswa {
    @Id
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "nama")
    private String name;

    @Column(name = "no_ujian")
    private String noUjian;

    @Column(name = "tahun")
    private Integer tahun;

    @ManyToOne
    @JoinColumn(name = "jenis_seleksi")
    private JenisSeleksi jenisSeleksi;

    @ManyToOne
    @JoinColumn(name = "jenjang")
    private Jenjang jenjang;

    @Column(name = "status_lulus")
    private Integer statusLulus;
}
