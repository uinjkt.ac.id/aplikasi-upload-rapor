package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "referensi", name = "mata_pelajaran")
@Data
public class MataPelajaran {
    @Id
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "kode", length = 10)
    private String kode;

    @Column(name = "nama", length = 255)
    private String nama;

}
