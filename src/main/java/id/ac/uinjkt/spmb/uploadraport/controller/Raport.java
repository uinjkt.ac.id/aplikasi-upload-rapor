package id.ac.uinjkt.spmb.uploadraport.controller;

import id.ac.uinjkt.spmb.uploadraport.dao.CalonMahasiswaDao;
import id.ac.uinjkt.spmb.uploadraport.dao.MataPelajaranDao;
import id.ac.uinjkt.spmb.uploadraport.dao.RaporCalonMahasiswaDao;
import id.ac.uinjkt.spmb.uploadraport.dao.TempNilaiDao;
import id.ac.uinjkt.spmb.uploadraport.dto.Nilai;
import id.ac.uinjkt.spmb.uploadraport.entity.CalonMahasiswa;
import id.ac.uinjkt.spmb.uploadraport.entity.RaporCalonMahasiswa;
import id.ac.uinjkt.spmb.uploadraport.entity.TempNilai;
import id.ac.uinjkt.spmb.uploadraport.helper.ExcelHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/raport")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class Raport {

    @Autowired
    private CalonMahasiswaDao calonMahasiswaDao;

    @Autowired
    private MataPelajaranDao mataPelajaranDao;

    @Autowired
    private TempNilaiDao tempNilaiDao;

    @Autowired
    private RaporCalonMahasiswaDao raporCalonMahasiswaDao;

    @GetMapping("/ping")
    public ResponseEntity<Object> ping(HttpServletRequest request) {

        return new ResponseEntity<>("ping berhasil", HttpStatus.OK);
    }

    @GetMapping("/calon")
    public ResponseEntity<Object> calonLimit(HttpServletRequest request, @RequestParam(name = "tahun", defaultValue = "2020") Integer tahun) {
        List<CalonMahasiswa> calonMahasiswaList = calonMahasiswaDao.findAllByTahun(tahun);

        //calonMahasiswaList.stream().limit(10);

        return new ResponseEntity<>(calonMahasiswaList, HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<Object> uplodNilai(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                log.info("file,  {}", file.getOriginalFilename());

                List<Nilai> nilais = ExcelHelper.excelToNilai(file.getInputStream());

                for (Nilai n : nilais) {
                    RaporCalonMahasiswa rapor = new RaporCalonMahasiswa();
                    rapor.setCalonMahasiswa(calonMahasiswaDao.findByNoUjian(n.getNoUjian()));
                    rapor.setSemester(Integer.parseInt(n.getSemester()));
                    rapor.setMataPelajaran(mataPelajaranDao.findByKode(n.getKode()));
                    rapor.setNilai(n.getNilai());

                    Optional<RaporCalonMahasiswa> oCalonMahasiswa = raporCalonMahasiswaDao.findByCalonMahasiswa_NoUjianAndMataPelajaran_KodeAndSemester(n.getNoUjian(), n.getKode(), Integer.parseInt(n.getSemester()));
                    if (!oCalonMahasiswa.isPresent()) {
                        //log.info("nilai ada: {}", n);
                        //} else {
                        log.info("nilai no: {}", n);
                        //raporCalonMahasiswaDao.save(rapor);
                    }
                }

                return ResponseEntity.status(HttpStatus.CREATED).body("Uploaded the file successfully: " + file.getOriginalFilename());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ex.toString());
            }
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please upload an excel file!");
    }

    @GetMapping("/proses")
    public ResponseEntity<Object> prosesNilai(HttpServletRequest request) {
        List<TempNilai> tempNilaiList = tempNilaiDao.findAll();
        for (TempNilai t : tempNilaiList) {


            RaporCalonMahasiswa rapor = new RaporCalonMahasiswa();
            rapor.setCalonMahasiswa(calonMahasiswaDao.findByNoUjian(t.getNoUjian()));
            rapor.setSemester(t.getSemester());
            rapor.setMataPelajaran(mataPelajaranDao.findByKode(t.getKode()));
            rapor.setNilai(t.getNilai());

            if (rapor.getCalonMahasiswa() != null && rapor.getMataPelajaran() != null) {
                log.info("Rapor: {}", rapor);
                raporCalonMahasiswaDao.save(rapor);
            }
        }

        return new ResponseEntity<>("Sukses", HttpStatus.OK);
    }
}
