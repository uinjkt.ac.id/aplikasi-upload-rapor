package id.ac.uinjkt.spmb.uploadraport.dao;

import id.ac.uinjkt.spmb.uploadraport.entity.CalonMahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CalonMahasiswaDao extends JpaRepository<CalonMahasiswa, Long> {
    List<CalonMahasiswa> findAllByTahun(@Param("tahun") Integer tahun);

    CalonMahasiswa findByNoUjian(String noUjian);
}
