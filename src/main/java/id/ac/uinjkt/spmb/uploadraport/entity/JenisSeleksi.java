package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "public", name = "jenis_seleksi")
@Data
public class JenisSeleksi {
    @Id
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "nama")
    private String name;
}
