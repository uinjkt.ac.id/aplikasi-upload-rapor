package id.ac.uinjkt.spmb.uploadraport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UploadRaportApplication {

	public static void main(String[] args) {
		SpringApplication.run(UploadRaportApplication.class, args);
	}

}
