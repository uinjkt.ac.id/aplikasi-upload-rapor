package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "public", name = "jenjang")
@Data
public class Jenjang {
    @Id
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "namanya")
    private String name;
}
