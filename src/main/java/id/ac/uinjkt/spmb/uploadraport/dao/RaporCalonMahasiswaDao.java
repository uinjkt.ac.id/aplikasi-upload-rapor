package id.ac.uinjkt.spmb.uploadraport.dao;

import id.ac.uinjkt.spmb.uploadraport.entity.RaporCalonMahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RaporCalonMahasiswaDao extends JpaRepository<RaporCalonMahasiswa, Long> {
    Optional<RaporCalonMahasiswa> findByCalonMahasiswa_NoUjianAndMataPelajaran_KodeAndSemester(String noUjian, String kode, Integer semester);
}
