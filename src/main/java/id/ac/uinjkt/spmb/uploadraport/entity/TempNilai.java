package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(schema = "pmb", name = "temp_nilai")
@Data
public class TempNilai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "no_ujian", length = 15)
    private String noUjian;

    @Column(name = "semester")
    private int semester;

    @Column(name = "kode", length = 10)
    private String kode;

    @Column(name = "nilai")
    private Double nilai;
}
