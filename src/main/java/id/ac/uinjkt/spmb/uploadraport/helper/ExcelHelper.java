package id.ac.uinjkt.spmb.uploadraport.helper;

import id.ac.uinjkt.spmb.uploadraport.dto.Nilai;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String SHEET = "nilai";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<Nilai> excelToNilai(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();


            List<Nilai> nilaiList = new ArrayList<>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                //log.info("rows: {}", rows);

                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();
                Nilai nilai = new Nilai();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();
                    switch (cellIdx) {
                        case 0:
                            nilai.setId((long) currentCell.getNumericCellValue());
                            break;

                        case 1:
                            nilai.setNoUjian(new BigDecimal(currentCell.getNumericCellValue()).toPlainString());
                            break;

                        case 2:
                            nilai.setSemester(new BigDecimal(currentCell.getNumericCellValue()).toPlainString());
                            break;
//
                        case 3:
                            nilai.setKode(currentCell.getStringCellValue());
                            break;
//
                        case 4:
                            nilai.setNilai(currentCell.getNumericCellValue());
                            break;

                        default:
                            break;
                    }
                    cellIdx++;
                }
                nilaiList.add(nilai);
                //log.info("nilai: {}", nilai);
            }

            workbook.close();
            return nilaiList;
        } catch (IOException e) {
            throw new RuntimeException("Fail to parse Excel file: " + e.getMessage());
        }
    }
}
