package id.ac.uinjkt.spmb.uploadraport.dao;

import id.ac.uinjkt.spmb.uploadraport.entity.MataPelajaran;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MataPelajaranDao extends JpaRepository<MataPelajaran, Long> {
    MataPelajaran findByKode(String kode);
}
