package id.ac.uinjkt.spmb.uploadraport.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(schema = "pmb", name = "rapor_calon_mahasiswa")
@Data
public class RaporCalonMahasiswa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "semester")
    private int semester;

    @Column(name = "nilai")
    private Double nilai;

    @ManyToOne
    @JoinColumn(name = "mata_pelajaran")
    private MataPelajaran mataPelajaran;

    @ManyToOne
    @JoinColumn(name = "calon_mahasiswa")
    private CalonMahasiswa calonMahasiswa;
}
